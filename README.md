# Acaisoft Recruitment Task  

## Comments  
In task description in mail I found bug.   
Curl without parameter -H and defined type(ex. "Content-Type: application/json") will exec request in format:  
  
    aplication/x-www-form-urlencoded  
  
For this format in Spring we can get values in PostMapping method by using  
  
    MultiValueMap 
  
But formatting must be correct. In email ex. request look like this:  
  
    curl -X POST localhost:8080/api/message -d ‘{“email”:“anna.zajkowska@example.com”,“title”:“Interview 3",“content”:“simple text 3",“magic_number”:101}’    
  
When we use MultiValueMap<String, String> or Map<String, String> this query will be loaded to Map under one key as one large String with all parameters names and values.  
I skipped cutting String and prepare controller to co-work with Json and correct write Urlencoded queries.  
Based on email I wrote 2 shell scripts.   
  
    json.sh  
  
and  
  
    urlencoded.sh  
  
Both execute request from email and on output write what is doing and what result of request we get.  
  
## Running      
* Clone this project      
* Make sure that you have enabled Annotation Processing and installed Lombok plugin
* In terminal go to location of file docker-compose.yaml (Acaisoft directory)  
* Run commands:   
    * mvn package    
    * docker-compose build    
    * docker-compose up         
* Or run start script:    
    * chmod +x start.sh    
    * ./start,sh    
## Scripts    
* Go to localization of json.sh and urlencoded.sh    
* Add execute permission for scripts (chmod +x {fileName.sh})    
* Execute them    

