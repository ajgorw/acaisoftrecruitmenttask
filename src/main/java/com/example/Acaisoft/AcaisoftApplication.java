package com.example.Acaisoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcaisoftApplication {

  public static void main(final String[] args) {
    SpringApplication.run(AcaisoftApplication.class, args);
  }
}
