package com.example.Acaisoft.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class MessageDto {
  private String email;
  private String title;
  private String content;
  private int magic_number;
}
