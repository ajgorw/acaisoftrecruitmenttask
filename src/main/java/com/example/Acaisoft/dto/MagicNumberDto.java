package com.example.Acaisoft.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MagicNumberDto {
  private Integer magic_number;
}
