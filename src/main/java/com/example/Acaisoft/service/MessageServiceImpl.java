package com.example.Acaisoft.service;

import com.example.Acaisoft.dto.MessageDto;
import com.example.Acaisoft.model.Message;
import com.example.Acaisoft.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.SliceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MessageServiceImpl implements MessageService {

  MessageRepository messageRepository;
  EmailSenderService emailSenderService;
  private static final int TTL_VALUE = 300;

  @Override
  public void saveMessageInDB(final MessageDto messageDto) {
    final Message entity =
        Message.builder()
            .UUID(UUID.randomUUID().toString())
            .title(messageDto.getTitle())
            .content(messageDto.getContent())
            .email(messageDto.getEmail())
            .magic_number(messageDto.getMagic_number())
            .build();
    messageRepository.saveWithTTL(entity, TTL_VALUE);
  }

  @Override
  public List<MessageDto> specifiedByEmail(final String email) {
    final List<Message> entities = messageRepository.findByEmail(email);
    return mapEntitiesToDtoList(entities);
  }

  @Override
  public void sendMessagesAndDeleteThemForSpecificMagicNumber(final int magic) {
    final List<Message> messageList = specifiedByMagicNumber(magic);
    final List<MessageDto> dtoList = mapEntitiesToDtoList(messageList);
    emailSenderService.sendEmails(dtoList);
    deleteMessages(messageList);
  }

  @Override
  public Slice<MessageDto> allForSpecifiedEmail(final String email, final Pageable pageable) {
    final Slice<Message> messageSlice = messageRepository.findAllByEmail(email, pageable);
    final List<MessageDto> dtoList = mapEntitiesToDtoList(messageSlice.getContent());
    return new SliceImpl<>(dtoList);
  }

  private List<Message> specifiedByMagicNumber(final int magic) {
    return messageRepository.findByMagicNumber(magic);
  }

  private void deleteMessages(final List<Message> messages) {
    messages.forEach(messageRepository::delete);
  }

  private List<MessageDto> mapEntitiesToDtoList(final List<Message> entities) {
    final ModelMapper modelMapper = new ModelMapper();
    return entities.stream()
        .map(entity -> modelMapper.map(entity, MessageDto.class))
        .collect(Collectors.toList());
  }
}
