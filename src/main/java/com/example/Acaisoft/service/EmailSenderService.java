package com.example.Acaisoft.service;

import com.example.Acaisoft.dto.MessageDto;

import java.util.List;

public interface EmailSenderService {
  void sendEmails(final List<MessageDto> dtoList);
}
