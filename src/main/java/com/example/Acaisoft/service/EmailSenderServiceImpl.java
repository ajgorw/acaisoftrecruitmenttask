package com.example.Acaisoft.service;

import com.example.Acaisoft.dto.MessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

@Service
public class EmailSenderServiceImpl implements EmailSenderService {
  @Autowired private JavaMailSender javaMailSender;

  @Value("${spring.mail.username}")
  private String username;

  @Value("${mail.replyEmail}")
  private String replyEmail;

  @Override
  public void sendEmails(final List<MessageDto> dtoList) {
    for (final MessageDto messageDto : dtoList) {
      sendEmailSpecifiedByMessage(messageDto);
    }
  }

  private void sendEmailSpecifiedByMessage(final MessageDto dto) {
    sendEmail(dto.getEmail(), dto.getTitle(), dto.getContent());
  }

  private void sendEmail(final String to, final String title, final String content) {
    final MimeMessage mail = javaMailSender.createMimeMessage();
    final MimeMessageHelper helper;
    try {
      helper = new MimeMessageHelper(mail, true);
      helper.setTo(to);
      helper.setReplyTo(replyEmail);
      helper.setFrom(username);
      helper.setSubject(title);
      helper.setText(content, true);
    } catch (final MessagingException e) {
      System.out.println(e.getMessage());
    }
    javaMailSender.send(mail);
  }
}
