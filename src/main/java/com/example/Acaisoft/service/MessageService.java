package com.example.Acaisoft.service;

import com.example.Acaisoft.dto.MessageDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import java.util.List;

public interface MessageService {
  void saveMessageInDB(final MessageDto messageDto);

  List<MessageDto> specifiedByEmail(String email);

  void sendMessagesAndDeleteThemForSpecificMagicNumber(int magic);

  Slice<MessageDto> allForSpecifiedEmail(String email, Pageable pageable);
}
