package com.example.Acaisoft.cassandra;

import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.datastax.driver.core.exceptions.TransportException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;

@Slf4j
public class RetryingConnection extends CassandraClusterFactoryBean {
  private static final org.slf4j.Logger LOG =
      org.slf4j.LoggerFactory.getLogger(RetryingConnection.class);

  @Override
  public void afterPropertiesSet() throws Exception {
    connect();
  }

  private void connect() throws Exception {
    try {
      super.afterPropertiesSet();
    } catch (final TransportException | IllegalArgumentException | NoHostAvailableException e) {
      LOG.warn(e.getMessage());
      LOG.warn("Delay 10s");
      sleep();
      connect();
    }
  }

  private void sleep() {
    try {
      Thread.sleep(10000);
    } catch (final InterruptedException ignored) {
    }
  }
}
