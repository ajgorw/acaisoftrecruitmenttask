package com.example.Acaisoft.cassandra;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.cql.keyspace.CreateKeyspaceSpecification;
import org.springframework.data.cassandra.core.cql.keyspace.KeyspaceOption;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

import java.util.List;

@Configuration
@EnableCassandraRepositories(basePackages = "com.example.Acaisoft.repository")
public class CassandraConfiguration extends AbstractCassandraConfiguration {

  @Value("${cassandra.contactpoints}")
  private String contactPoints;

  @Value("${cassandra.keyspace}")
  private String keySpace;

  @Value("${cassandra.basePackages}")
  private String basePackages;

  @Override
  protected String getKeyspaceName() {
    return keySpace;
  }

  @Override
  protected String getContactPoints() {
    return contactPoints;
  }

  @Override
  protected List<CreateKeyspaceSpecification> getKeyspaceCreations() {
    final CreateKeyspaceSpecification specification =
        CreateKeyspaceSpecification.createKeyspace(keySpace)
            .ifNotExists()
            .with(KeyspaceOption.DURABLE_WRITES, true)
            .withSimpleReplication();
    return List.of(specification);
  }

  @Override
  protected List<String> getStartupScripts() {
    final String create =
        String.format(
            "CREATE TABLE IF NOT EXISTS %s.message ("
                + "    UUID text PRIMARY KEY,"
                + "    magic_number int,"
                + "    email text,"
                + "    content text,"
                + "    title text)",
            keySpace);
    final String magicNumberIdx =
        String.format(
            "CREATE INDEX IF NOT EXISTS idx_magic_number ON %s.message(magic_number)", keySpace);
    final String emailIdx =
        String.format("CREATE INDEX IF NOT EXISTS idx_email ON %s.message(email)", keySpace);
    return List.of(create, magicNumberIdx, emailIdx);
  }

  @Override
  public SchemaAction getSchemaAction() {
    return SchemaAction.CREATE_IF_NOT_EXISTS;
  }

  @Override
  public String[] getEntityBasePackages() {
    return new String[] {basePackages};
  }

  @Bean
  @Override
  public CassandraClusterFactoryBean cluster() {
    final RetryingConnection retryingConnection = new RetryingConnection();
    retryingConnection.setAddressTranslator(getAddressTranslator());
    retryingConnection.setAuthProvider(getAuthProvider());
    retryingConnection.setClusterBuilderConfigurer(getClusterBuilderConfigurer());
    retryingConnection.setClusterName(getClusterName());
    retryingConnection.setCompressionType(getCompressionType());
    retryingConnection.setContactPoints(getContactPoints());
    retryingConnection.setLoadBalancingPolicy(getLoadBalancingPolicy());
    retryingConnection.setMaxSchemaAgreementWaitSeconds(getMaxSchemaAgreementWaitSeconds());
    retryingConnection.setMetricsEnabled(getMetricsEnabled());
    retryingConnection.setNettyOptions(getNettyOptions());
    retryingConnection.setPoolingOptions(getPoolingOptions());
    retryingConnection.setPort(getPort());
    retryingConnection.setProtocolVersion(getProtocolVersion());
    retryingConnection.setQueryOptions(getQueryOptions());
    retryingConnection.setReconnectionPolicy(getReconnectionPolicy());
    retryingConnection.setRetryPolicy(getRetryPolicy());
    retryingConnection.setSpeculativeExecutionPolicy(getSpeculativeExecutionPolicy());
    retryingConnection.setSocketOptions(getSocketOptions());
    retryingConnection.setTimestampGenerator(getTimestampGenerator());
    retryingConnection.setKeyspaceCreations(getKeyspaceCreations());
    retryingConnection.setKeyspaceDrops(getKeyspaceDrops());
    retryingConnection.setStartupScripts(getStartupScripts());
    retryingConnection.setShutdownScripts(getShutdownScripts());

    return retryingConnection;
  }
}
