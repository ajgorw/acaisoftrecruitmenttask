package com.example.Acaisoft.model;

import lombok.*;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@Table
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Message {

  @PrimaryKey public String UUID;
  private String email;
  private int magic_number;
  private String title;
  private String content;
}
