package com.example.Acaisoft.repository;

import com.example.Acaisoft.model.Message;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageRepository extends CrudRepository<Message, String> {

  Slice<Message> findAllByEmail(String email, Pageable pageRequest);

  @Query(
      value =
          "INSERT INTO message "
              + "(UUID, content, email,magic_number, title) "
              + "VALUES ("
              + ":#{#message.UUID},"
              + ":#{#message.content},"
              + ":#{#message.email},"
              + ":#{#message.magic_number},"
              + ":#{#message.title})"
              + " USING TTL :TtlValue")
  void saveWithTTL(@Param("message") Message message, @Param("TtlValue") int TtlValue);

  @Query(value = "select * from message where magic_number=?0")
  List<Message> findByMagicNumber(int magic);

  @Query(value = "select * from message where email=?0")
  List<Message> findByEmail(String email);
}
