package com.example.Acaisoft.controller;

import com.example.Acaisoft.dto.MagicNumberDto;
import com.example.Acaisoft.dto.MessageDto;
import com.example.Acaisoft.repository.MessageRepository;
import com.example.Acaisoft.service.EmailSenderService;
import com.example.Acaisoft.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.data.cassandra.core.query.CassandraPageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("api")
@AllArgsConstructor
public class ApiController {
  MessageService messageService;
  EmailSenderService emailSenderService;
  MessageRepository messageRepository;

  @PostMapping(path = "/message", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<MessageDto> postMessage(@RequestBody final MessageDto messageDto) {
    messageService.saveMessageInDB(messageDto);
    return new ResponseEntity<>(messageDto, HttpStatus.OK);
  }

  @PostMapping(path = "/message", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  public ResponseEntity<MessageDto> postMessage(
      @RequestParam final MultiValueMap<String, String> map) {
    final MessageDto messageDto =
        MessageDto.builder()
            .email(map.getFirst("email"))
            .content(map.getFirst("content"))
            .magic_number(Integer.parseInt(Objects.requireNonNull(map.getFirst("magic_number"))))
            .title(map.getFirst("title"))
            .build();
    messageService.saveMessageInDB(messageDto);
    return new ResponseEntity<>(messageDto, HttpStatus.OK);
  }

  @PostMapping(path = "/send", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> sendMessage(@RequestBody final MagicNumberDto magicNumberDto) {
    messageService.sendMessagesAndDeleteThemForSpecificMagicNumber(
        magicNumberDto.getMagic_number());
    return new ResponseEntity<>("Sent messages", HttpStatus.OK);
  }

  @PostMapping(path = "/send", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  public ResponseEntity<String> sendMessage(
      @RequestParam final MultiValueMap<String, String> params) {
    final int magic = Integer.parseInt(Objects.requireNonNull(params.getFirst("magic_number")));
    messageService.sendMessagesAndDeleteThemForSpecificMagicNumber(magic);
    return new ResponseEntity<>("Sent messages", HttpStatus.OK);
  }

  @GetMapping(value = "/messages/{emailValue}/{page}/{size}")
  public ResponseEntity<List<MessageDto>> getPaginated(
      @PathVariable("emailValue") final String emailValue,
      @PathVariable("page") final Integer page,
      @PathVariable("size") final int size) {
    Slice<MessageDto> resultList =
        messageService.allForSpecifiedEmail(emailValue, CassandraPageRequest.first(size));
    int currentPage = 0;
    while (resultList.hasNext() && currentPage <= page) {
      resultList = messageService.allForSpecifiedEmail(emailValue, resultList.nextPageable());
      currentPage++;
    }

    return ResponseEntity.ok(resultList.getContent());
  }
}
