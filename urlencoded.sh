#!/bin/bash
echo "3x Post"
curl -H 'Content-Type: application/x-www-form-urlencoded' -X POST http://localhost:8080/api/message -d 'email=jan.kowalski@example.com&title=Interview&content=simple text&magic_number=101' && echo &&
curl -H 'Content-Type: application/x-www-form-urlencoded' -X POST http://localhost:8080/api/message -d 'email=jan.kowalski@example.com&title=Interview 2&content=simple text 2&magic_number=22' && echo &&
curl -H 'Content-Type: application/x-www-form-urlencoded' -X POST http://localhost:8080/api/message -d 'email=anna.zajkowska@example.com&title=Interview 3&content=simple text 3&magic_number=101' && echo &&
echo "Now pagination for jan first page 2 items:"
curl -H 'Content-Type: application/x-www-form-urlencoded' -X GET localhost:8080/api/messages/jan.kowalski@example.com/1/2 && echo &&
echo "Now pagination for jan first page 1 items:"
curl -H 'Content-Type: application/x-www-form-urlencoded' -X GET localhost:8080/api/messages/jan.kowalski@example.com/1/1 && echo &&
echo "Now pagination for anna first page 1 items:"
curl -H 'Content-Type: application/x-www-form-urlencoded' -X GET localhost:8080/api/messages/anna.zajkowska@example.com/1/1 && echo &&
echo "Send for magic number==101"
curl -H 'Content-Type: application/x-www-form-urlencoded' -X POST localhost:8080/api/send -d 'magic_number=101' && echo &&
echo "Now  pagination for jan first page 2 items:"
curl -H 'Content-Type: application/x-www-form-urlencoded' -X GET localhost:8080/api/messages/jan.kowalski@example.com/1/2 && echo &&
echo "Now get pagination for anna with 1 page 1 items:" &&
curl -H 'Content-Type: application/x-www-form-urlencoded' -X GET localhost:8080/api/messages/anna.zajkowska@example.com/1/1